				   /*
+---------------------------------------+
| Programm: Precious Item Security      |
| Functions used:init()initall()        |
|          display() delay()            |
| Author  :  Pulkit Gupta               |
| Email: pulkit.itp@gmail.com           |
+---------------------------------------+
*/			  


#include <reg52.H>  //Include file for 8051

#define IdLight P1


sbit alarm = P0^7;
sbit inputSignal = P0^0;

sbit l1 = P0^1;
sbit l2 = P0^2;
sbit l3 = P0^3;
sbit l4 = P0^4;


void init()
{
	P1 =0x00;
	alarm =0;
}

void main()
{
	P1 =0x00;
	alarm =0;
	inputSignal =1;
	l1=l2=l3=l4=0;
	//init();
	while(1)
	{
		alarm = !inputSignal;
		l1 =l2 = l3 = l4 = alarm;
		
	}
	

}


