				   /*
+---------------------------------------+
| Programm: Security Systrem		   |
| Functions used:init()initall()        |
|          display() delay()            |
| Author  :  Pulkit Gupta               |
| Email: pulkit.itp@gmail.com           |
+---------------------------------------+
*/			   //unconnected p3.  345

#include <reg52.H>  //Include file for 8051

#define PASS 1234
#define keyport P0  //keypad connected to P2

sbit  col1 =P0^0;   //column 1
sbit  col2 =P0^1;   //column 2
sbit  col3 =P0^2;   //column 3
sbit  col4 =P0^3;   //column 4

sbit  ones =P2^7;   //PORT  FOR SWITCHING ONES AND TENS



sbit a = P3^1;	    //seven segments
sbit b = P3^2;
sbit c = P3^3;
sbit d = P3^4;
sbit e = P3^5;
sbit f = P3^6;
sbit g = P3^7;


sbit signal=P2^5;

sbit sw =P2^2;



sbit motor =P2^0;
sbit buzzer =P2^1;
/*
+---------------------------------------+
| Prototype: void init(void);           |
| Return Type: void                     |
| Arguments: None                       |
| Description: Initialize ports and     |
|              Keypad.                  |
+---------------------------------------+
*/
void init(int x)
{
	a=x;
	b=x;
	c=x;
	d=x;
	e=x;
	f=x;
	g=x;
}
void print(int n);

void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<110;j++)
		{}
	}
}



void display(int n,long msec)  // CURRENTLY NOT USED 
{

	int i=0;
	for(i=0;i<msec;i++)
	{
		int t=n/10;
		int o=n%10;
		//tens=1;
		ones=0;
		print(t);
		//delay(12);
		//tens=0;
		ones=1;
		print(o);
		//delay(12);
	}
}


void printa(char a)
{
	ones=1;
	if(a=='P')
	{
		init(1);
		c=d=0;
	}
	if(a=='U')
	{
		init(1);
		a=g=0;
	}
	 if(a=='L')
	{
		init(1);
		a=0;
		b=c=g=0;
	}
}

void print(int n)
{
	switch(n)
	{
		case 0:
		init(1);
		g=0;
		break;

		case 1:
		init(0);
		b=c=1;
		break;

		case 2:
		init(1);
		c=f=0;
		break;

		case 3:
		init(1);
		e=f=0;
		break;

		case 4:
		init(1);
		a=d=e=0;
		break;

		case 5:
		init(1);
		b=e=0;
		break;

		case 6:
		init(1);
		b=0;
		break;

		case 7:
		init(0);
		a=b=c=1;
		break;

		case 8:
		init(1);
		break;

		case 9:
		init(1);
		e=0;
		break;

	} //switch

}





void initall()
{
		signal=0;
         //keyport =0x0F; //make Rows as o/p and cols are i/p
		col1=1;
		col2=1;
		col3=1;
		col4=1;

		sw=1;


		ones=0;
		motor=0;
		buzzer=0;
}

/*
+-----------------------------------------------+
| Prototype: unsigned char key_input(void);       |
| Return Type: unsigned char                    |
| Arguments: None  key=0 not pressed            |
| Description: To read key from the keypad      |
+-----------------------------------------------+
*/
unsigned char key_input()
{
        unsigned char i,k,key=0;
        k=1;
        for(i=0;i<4;i++)
		{

	      keyport =~(0x80>>i);    //!1000000 =01111111  make 0 on every pin of row
		  if(col1==0)
			{
				signal=1;
                key = k+0;

                while(col1==0);
                signal=0;

                return key;
            }
              if(col2==0)
			{
				signal=1;
				key = k+1;
				while(col2==0);
				signal=0;

				return key;
			}
              if(col3==0)
			{
				signal=1;
				key = k+2;
				while(col3==0);
				signal=0;

				return key;
            }
            if(col4==0)
			{
				signal=1;
                                key = k+3;
                                while(col4==0);
                                signal=0;
								return key;
                        }
                k+=4;
                keyport |= 0x80>>i;
        }
        return 0;
}


int place=1;
int entered[4];

void i(int f[])
{
	int l=0;
	for(l=0;l<4;l++)
	f[l]=0;
}

unsigned char key;
long number=0;
int unlocked=0;

void main()
{
	start:
	i(entered);
	unlocked=0;
	key =0;
	place=0;
	initall();
	while(1)
	{
		if(sw!=0&&place<4&&unlocked==0)
		{
			key = key_input();
			if(key!=0)	 //button pressed
			{
				ones=1;
				//display(key-1,40);
				
				print(key-1);
				entered[place]=key-1;
				place++;
			}
			
		}
		if(sw==0&&unlocked==0)
		{
			place=0;
			signal=1;
			while(!sw);
			signal=0;

			ones=0;
			number=1000*entered[0]+100*entered[1]+10*entered[2]+entered[3];
			if(entered[3]==4)
			{
				//printa('U');
				ones=1;
				init(1);
				a=g=0;
				motor=1;
				delay(10000);
				unlocked=1;
			}
			else
			{
				motor=0;
				buzzer = 1;	
				delay(3000);
				buzzer = 0;
				delay(2000);
				buzzer = 1;
				delay(3000);
				buzzer = 0;
				delay(2000);
				buzzer = 1;
				delay(3000);
				buzzer = 0;
				
			}
		}

		
	}//while
}
