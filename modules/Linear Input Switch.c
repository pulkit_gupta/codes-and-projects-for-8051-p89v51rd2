				   /*
+---------------------------------------+
| Program: Linear Input Switch          |
|                                       |
|                                       |
| Author  :  Pulkit                     |
| Email : pulkit.itp@gmail.com          |
+---------------------------------------+
*/			   

#include <reg52.H>  //Include file for 8051


sbit swtch = P0^3;
sbit led = P0^0;

void init()
{
	swtch = 1;
}

void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<110;j++)    //calibrated
		{}
	}

}
void main()
{
	init();
	while(1)
	{
		led = !swtch;
	}
}
