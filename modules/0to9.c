/*
+---------------------------------------+
| Programm: 0 to 9 count                |
|                                       |
|                                       |
| Author  :  Pulkit                     |
| Email: pulkit.itp@gmail.com           |
+---------------------------------------+
*/

#include <REG52.H>

#ifdef MONITOR51                         /* Debugging with Monitor-51 needs   */
char code reserve [3] _at_ 0x23;         /* space for serial interrupt if     */
#endif                                   /* Stop Exection with Serial Intr.   */
                                         /* is enabled                        */
/*------------------------------------------------
The main C function.  Program execution starts
here after stack initialization.
------------------------------------------------*/
void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		/*
		if(sminus==0||splus==0||flag==1)
		{break;
		} */
		for(j=0;j<110;j++)
		{
			/*if(sminus==0||splus==0)
			{
				flag=1;
				break;
			} unomment these if wanna great response for your swith*/

		}
	}
}


sbit a = P0^0;	  //seven segments
sbit b = P0^1;
sbit c = P0^2;
sbit d = P0^3;
sbit e = P0^4;
sbit f = P0^5;
sbit g = P0^6;

void init(int x)
{
	a=x;
	b=x;
	c=x;
	d=x;
	e=x;
	f=x;
	g=x;
}



void display(int n)
{
	switch(n)
	{
		case 0:
		init(1);
		g=0;
		break;

		case 1:
		init(0);
		b=c=1;
		break;

		case 2:
		init(1);
		c=f=0;
		break;

		case 3:
		init(1);
		e=f=0;
		break;

		case 4:
		init(1);
		a=d=e=0;
		break;

		case 5:
		init(1);
		b=e=0;
		break;

		case 6:
		init(1);
		b=0;
		break;

		case 7:
		init(0);
		a=b=c=1;
		break;

		case 8:
		init(1);
		break;

		case 9:
		init(1);
		e=0;
		break;

	}
}

int n=0;
void main (void)
{

/*------------------------------------------------
Setup the serial port for 1200 baud at 16MHz.
------------------------------------------------*/
#ifndef MONITOR51
    SCON  = 0x50;		        /* SCON: mode 1, 8-bit UART, enable rcvr      */
    TMOD |= 0x20;               /* TMOD: timer 1, mode 2, 8-bit reload        */
    TH1   = 221;                /* TH1:  reload value for 1200 baud @ 16MHz   */
    TR1   = 1;                  /* TR1:  timer 1 run                          */
    TI    = 1;                  /* TI:   set TI to send first char of UART    */
#endif

/*------------------------------------------------
Note that an embedded program never exits (because
there is no operating system to return to).  It
must loop and execute forever.
------------------------------------------------*/
init(0);

while(1)
{

display(n);
delay(1000);
n++;
if(n==10)
n=0;
}

}


