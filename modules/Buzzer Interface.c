/*
+---------------------------------------+
| Program: Buzzer Interface             |
|                                       |
|                                       |
| Author  :  Pulkit                     |
| Email : pulkit.itp@gmail.com          |
+---------------------------------------+
*/			   

#include <reg52.H>  //Include file for 8051


sbit buzzer = P0^0;

void init()
{
	buzzer = 0;
}

void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<110;j++)    //calibrated
		{}
	}

}

int i = 0;
void main()
{
	init();
	while(1)
	{
		buzzer = 1;
	}
}
