/***************************************************************
Prog: To check external pulse on p3.2 INT0
Author: Pulkit
****************************************************************/

#include <REG52.H>
unsigned char counter = 0;

sbit l1 = P1^1;

void ex0_isr (void) interrupt 0
{
	counter++;   
	l1=!l1;
}

void main (void)
{
	IT0 = 1;   // Configure interrupt 0 for falling edge on /INT0 (P3.2)
	EX0 = 1;   // Enable EX0 Interrupt
	EA = 1;    // Enable Global Interrupt Flag
	l1=1;
	while (1)
  	{
  	}	
}

/*=============================================================================
=============================================================================*/
