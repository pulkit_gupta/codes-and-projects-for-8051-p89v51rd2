				   /*
+---------------------------------------+
| Program: Toggling through Switch      |
|                                       |
|                                       |
| Author  :  Pulkit                     |
|                                       |
+---------------------------------------+
*/			   

#include <reg52.H>  //Include file for 8051


sbit swtch = P0^3;
sbit led = P0^0;

void init()
{
	swtch = 1;
}

void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<110;j++)    //calibrated
		{}
	}

}

int i = 0;
void main()
{
	init();
	while(1)
	{
		if(swtch ==0)
		{
			i++;
		}
		if(i%2==0)
		{
			led =1;
		}
		if(i%2!=0)
		{
			led=0;
		}
	}
}
