				   /*
+---------------------------------------+
| Programm: Voting System               |
| Functions used:init()initall()        |
|          display() delay()            |
| Author  :  Pulkit Gupta               |
| Email: pulkit.itp@gmail.com           |
+---------------------------------------+
*/			   //unconnected p3.  345

#include <reg52.H>  //Include file for 8051

sbit a = P3^1;	    //seven segments
sbit b = P3^2;
sbit c = P3^3;
sbit d = P3^4;
sbit e = P3^5;
sbit f = P3^6;
sbit g = P3^7;


sbit signal=P0^7;

sbit partyA =P0^0;
sbit partyB =P0^1;
sbit result = P0^2;

sbit buzzer = P0^4;

void init(int x)
{
	a=x;
	b=x;
	c=x;
	d=x;
	e=x;
	f=x;
	g=x;
}
void print(int n);

void delay(unsigned int s)
{
	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<110;j++)
		{}
	}
}



void display(int n,long msec)  // CURRENTLY NOT USED 
{

	int i=0;
	for(i=0;i<msec;i++)
	{
		int t=n/10;
		int o=n%10;
		//tens=1;
	
		//delay(12);
		//tens=0;
		print(o);
		//delay(12);
	}
}


void printa(int ch)
{
	if(ch==0)
	{
		init(1);
		d=0;   //a
	}
	if(ch==1)
	{
		init(1);
		a=b=0;     //b
	}
	if(ch==2)
	{
		init(1);  //c
		b=c=g=0;
	}
	if(ch==3)
	{
		init(1);
		a=f=0;     // d
	}
}

void print(int n)
{
	switch(n)
	{
		case 0:
		init(1);
		g=0;
		break;

		case 1:
		init(0);
		b=c=1;
		break;

		case 2:
		init(1);
		c=f=0;
		break;

		case 3:
		init(1);
		e=f=0;
		break;

		case 4:
		init(1);
		a=d=e=0;
		break;

		case 5:
		init(1);
		b=e=0;
		break;

		case 6:
		init(1);
		b=0;
		break;

		case 7:
		init(0);
		a=b=c=1;
		break;

		case 8:
		init(1);
		break;

		case 9:
		init(1);
		e=0;
		break;

	} //switch

}





void initall()
{
		signal=0;
		partyA = 1;
		partyB = 1;
		result = 1;
		buzzer = 0;
}

int ac =0 ;
int bc =0 ;

void main()
{
	buzzer =0;
//	initall(); // if you wanna initialize all
	while(1)
	{
		if(partyA ==0)
		{
			signal =1;
			while(partyA ==0);
			ac++;
			if(ac==10)
			{	
				printa(0);
				buzzer = 1;
				while(1);
			}
			print(ac);
			signal = 0;
		}
		if(partyB ==0)
		{
			signal =1;
			while(partyB ==0);
			bc++;
			if(bc==10)
			{	
				printa(1);
				buzzer = 1;
				while(1);
			}
			print(bc);
			signal = 0;
		}
		
		if(result ==0)
		{
			signal =1;
			while(result ==0);

			if(ac>bc)
			{
				printa(0);
				buzzer = 1;
				while(1);
			}
			if(bc>ac)
			{
				printa(1);
				buzzer = 1;
				while(1);
			}
			
			signal = 0;
		}					
		


		
	}

}
