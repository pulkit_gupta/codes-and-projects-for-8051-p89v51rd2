//////////////////////////////////////
//									//
//Program: Traffic Light			//
//									//		
//Author: Pulkit Gupta				//		
// Email: pulkit.itp@gmail.com      //
/////////////////////////////////////



#include<reg52.h>

///////////////////////////////////////////
										  //
										  //

sbit red1 = P0^0;
sbit yellow1 = P0^2;
sbit green1 = P0^1;

sbit red2 = P1^5;
sbit yellow2 = P1^3;
sbit green2 = P1^4;

sbit red3 = P0^6;
sbit yellow3 = P0^5;
sbit green3 = P0^4;

sbit red4 = P1^2;
sbit yellow4 = P1^1;
sbit green4 = P1^0;
////////////////////////////////////////////
/*void todelay()
{
	TMOD=0x01;
	TL0=0xdc;
	TH0=0x00;
	TR0=1;
	while(TF0==0);
	TR0=0;
	TF0=0;
}
void delay(int sec)
{
	int i=0;
	while(i<sec)
	{
		todelay();
	}

}	*/

int aryellow[4], arred[4], argreen[4];
void delay(unsigned int s)
{

	unsigned int i,j;
	for(i=0;i<s;i++)
	{
		for(j=0;j<100;j++)
		{}
	}

}

void makegreen(int i)
{
	int p;
	for(p=0;p<4;p++)
	{
		argreen[p] =0;
		arred[p] = 1;
		if(i==p)
		{
			argreen[p] = 1;					
			arred[p] = 0;
		}
	}

	red1 = arred[0];
	red2 = arred[1];
	red3 = arred[2];
	red4 = arred[3];
	
	green1 = argreen[0];
	green2 = argreen[1];
	green3 = argreen[2];
	green4 = argreen[3];
	
	
	
	delay(2000);  //total time for one state
	delay(2000);
	delay(2000);
	delay(2000);
	delay(2000);


}




void initall()
{
	int p;
	green1 = green2 = green3 = green4 = 0; 		 		
	red1 = red2 = red3 = red4=0;
	yellow1 = yellow2 = yellow3 = yellow4 = 0;
	for(p = 0; p<4;p++)
	{
		arred[p]=0;
		aryellow[p]=0;
		argreen[p]=0;
	}
}


void main()
{
	int fact = 0;
	int trans = 3;
	initall();
	while(1)
	{
		makegreen(fact);
		
	
		aryellow[fact] = 1;
		argreen[fact]=0;
		yellow1 = aryellow[0];
		yellow2 = aryellow[1];
		yellow3 = aryellow[2];		
		yellow4 = aryellow[3];
		green1 = argreen[0];
		green2 = argreen[1];
		green3 = argreen[2];
		green4 = argreen[3];

		delay(2000);        // transition time


		aryellow[fact] = 0;
		yellow1 = aryellow[0];
		yellow2 = aryellow[1];
		yellow3 = aryellow[2];		
		yellow4 = aryellow[3];

		fact++;
		fact =fact%4;
	}


}


